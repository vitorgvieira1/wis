var addErrorMessage = true;

function redirect(){
    window.location.href = "index.html";
}

function login(){
    var data = $("form").serialize();
    jQuery.support.cors = true;
    $.ajax({
        type : 'POST',
        headers: {
            "Accept":"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Accept-Encoding":"gzip, deflate, sdch",
            "Accept-Language":"pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4",
            "Connection":"keep-alive",
            "Cookie":"PHPSESSID=d241c622cf0b600a99aae7f1d7d0e2a5",
            "Host":"vitorgv.com.br",
            "Upgrade-Insecure-Requests":"1",
            "User-Agent":"Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.76 Mobile Safari/537.36"
        },
        dataType: 'json',
        url  : 'http://vitorgv.com.br/php/login.php',
        data : data,
        success :  function(data){
           var json = jQuery.parseJSON(data);
           var senha = $("#password").val();
           if (json.password == senha){
              var user = {email: email, password: senha}
              window.localStorage.setItem("login", user);
              searchUser();
           } else {
               if (addErrorMessage){
                   $("#errorMessage").append("<p style='color: red; id='errorMessageText' text-align: right; margin-right: 1%;'> Email ou senha invalidos </p>")
                   addErrorMessage = false;
               }
           }
        }, error: function (request, error) {
           console.log(arguments);
           alert(" Can't do because: " + error);
        }
    });
}

function searchUser(){
    var user = window.localStorage.getItem("login");
    if (user){
        redirect();
    }
}